package com.senler.ayca.earthquake

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.earthquake.adapter.EarthquakeListAdapter
import com.senler.ayca.earthquake.model.Features
import com.senler.ayca.earthquake.model.MainData
import com.senler.ayca.earthquake.retrofit.ApiClient
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.Pulse
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.filter_dialog_design.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListEarthquakeActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var earthquakeList: List<Features>
    lateinit var earthquakeListAdapter: EarthquakeListAdapter
    lateinit var earthquakeRecyclerView: RecyclerView
    lateinit var filterBtn: ImageButton
    lateinit var mapBtn: ImageButton
    lateinit var progressBar: ProgressBar
    lateinit var blurLayout: LinearLayout
    lateinit var adView: AdView

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list_earthquake_activity)
        initView()
        initAd()
    }

    private fun initAd() {

        MobileAds.initialize(this, "ca-app-pub-1898235865477290~5936104495")
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun initView() {
        adView = findViewById(R.id.ad_banner_list_earthquake_activity)
        earthquakeList = mutableListOf()
        earthquakeRecyclerView = findViewById(R.id.earthquake_recycler_view)
        earthquakeRecyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        filterBtn = findViewById(R.id.filter_btn)
        mapBtn = findViewById(R.id.map_btn)
        progressBar = findViewById(R.id.spin_kit)
        blurLayout = findViewById(R.id.blur_layout)

        val pulse: Sprite = Pulse()
        progressBar.setIndeterminateDrawableTiled(pulse)

        filterBtn.setOnClickListener(this)
        mapBtn.setOnClickListener(this)
        getEarthquake()
    }

    fun getEarthquake() {
        val call: Call<MainData> = ApiClient.getClient.getEarthquake()
        call.enqueue(object : Callback<MainData> {
            override fun onFailure(call: Call<MainData>?, t: Throwable?) {
                Log.d("ListEarthquakeActivity", "onFail")
            }

            override fun onResponse(call: Call<MainData>?, response: Response<MainData>?) =
                if (response!!.isSuccessful) {
                    earthquakeList = response.body().features
                    earthquakeListAdapter =
                        EarthquakeListAdapter(earthquakeList, this@ListEarthquakeActivity)
                    earthquakeRecyclerView.adapter = earthquakeListAdapter
                    blurLayout.visibility = View.GONE
                } else {

                }

        }
        )
    }

    fun getEarthquakeByMag(mag: Double) {
        val call: Call<MainData> = ApiClient.getClient.getEarthquakeByMag(mag)
        call.enqueue(object : Callback<MainData> {
            override fun onFailure(call: Call<MainData>?, t: Throwable?) {
                Log.d("ListEarthquakeActivity", "onFail")
            }

            override fun onResponse(call: Call<MainData>?, response: Response<MainData>?) =
                if (response!!.isSuccessful) {
                    earthquakeList = response.body().features
                    if (earthquakeList.isEmpty()) {
                        Toast.makeText(this@ListEarthquakeActivity, "No Value", Toast.LENGTH_SHORT)
                            .show()
                        customDialog()
                    }
                    earthquakeListAdapter =
                        EarthquakeListAdapter(earthquakeList, this@ListEarthquakeActivity)
                    earthquakeRecyclerView.adapter = earthquakeListAdapter
                    blurLayout.visibility = View.GONE
                } else {

                }

        }
        )
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            filterBtn.id -> {
                customDialog()
            }
            mapBtn.id -> {
                onBackPressed()
            }
        }

    }

    private fun customDialog() {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.filter_dialog_design, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        mDialogView.two_plus_btn.setOnClickListener {
            getEarthquakeByMag(2.0)
            mAlertDialog.dismiss()
        }
        mDialogView.three_plus_btn.setOnClickListener {
            getEarthquakeByMag(3.0)
            blurLayout.visibility = View.VISIBLE
            mAlertDialog.dismiss()
        }
        mDialogView.four_plus_btn.setOnClickListener {
            getEarthquakeByMag(4.0)
            blurLayout.visibility = View.VISIBLE
            mAlertDialog.dismiss()
        }
        mDialogView.five_plus_btn.setOnClickListener {
            getEarthquakeByMag(5.0)
            blurLayout.visibility = View.VISIBLE
            mAlertDialog.dismiss()
        }
        mDialogView.six_plus_btn.setOnClickListener {
            getEarthquakeByMag(6.0)
            blurLayout.visibility = View.VISIBLE
            mAlertDialog.dismiss()
        }
        mDialogView.seven_plus_btn.setOnClickListener {
            getEarthquakeByMag(7.0)
            blurLayout.visibility = View.VISIBLE
            mAlertDialog.dismiss()
        }
        mDialogView.eight_plus_btn.setOnClickListener {
            getEarthquakeByMag(8.0)
            blurLayout.visibility = View.VISIBLE
            mAlertDialog.dismiss()
        }
        mDialogView.nine_plus_btn.setOnClickListener {
            getEarthquakeByMag(9.0)
            blurLayout.visibility = View.VISIBLE
            mAlertDialog.dismiss()
        }
        mDialogView.cancel_btn.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }
}