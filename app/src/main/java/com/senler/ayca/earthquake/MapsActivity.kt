package com.senler.ayca.earthquake

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.Resources
import android.location.Address
import android.location.Geocoder
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.annotation.RequiresApi
import com.senler.ayca.earthquake.model.Features
import com.senler.ayca.earthquake.model.MainData
import com.senler.ayca.earthquake.retrofit.ApiClient
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.Pulse
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.maps.android.clustering.ClusterItem
import com.google.maps.android.clustering.ClusterManager
import android.content.pm.PackageManager
import android.view.LayoutInflater
import androidx.activity.OnBackPressedCallback
import kotlinx.android.synthetic.main.bottom_sheet_dialog.view.*
import kotlinx.android.synthetic.main.custom_dialog_design.view.*

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener,
    View.OnClickListener {

    private lateinit var mMap: GoogleMap
    private lateinit var goBtn: ImageButton
    private lateinit var mapTypeBtn: Button
    private lateinit var clusterManager: ClusterManager<ClusterItem>
    private val markerList = mutableListOf<MarkerOptions>()
    var featuresList: List<Features> = mutableListOf()
    lateinit var mapFragment: SupportMapFragment
    lateinit var listBtn: ImageButton
    lateinit var progressBar: ProgressBar
    lateinit var blurLayout: LinearLayout
    lateinit var adView: AdView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.maps_activity)
        getEarthquake()
        initView()
        initAd()


//        val zoom = findViewById(R.id.zoom) as ZoomControls
//        zoom.setOnZoomOutClickListener { mMap.animateCamera(CameraUpdateFactory.zoomOut()) }
//        zoom.setOnZoomInClickListener { mMap.animateCamera(CameraUpdateFactory.zoomIn()) }


    }

    private fun initAd() {
        MobileAds.initialize(this, "ca-app-pub-1898235865477290~5936104495")
        val adRequest = AdRequest.Builder().build()
        adView?.loadAd(adRequest)
    }


    private fun initView() {
        adView = findViewById(R.id.ad_banner_maps_activity)
        mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        listBtn = findViewById(R.id.list_btn)
        goBtn = findViewById(R.id.go_btn)
        progressBar = findViewById(R.id.spin_kit)
        blurLayout = findViewById(R.id.blur_layout)

        val pulse: Sprite = Pulse()
        progressBar.setIndeterminateDrawableTiled(pulse)

        listBtn.setOnClickListener(this)
        goBtn.setOnClickListener(this)
    }

    fun getEarthquake() {
        val call: Call<MainData> = ApiClient.getClient.getEarthquake()
        call.enqueue(object : Callback<MainData> {
            override fun onFailure(call: Call<MainData>?, t: Throwable?) {
                Log.d("MapsActivity", "onFail")
            }

            override fun onResponse(call: Call<MainData>?, response: Response<MainData>?) =
                if (response!!.isSuccessful) {

                    featuresList = response.body().features

                    var point: LatLng?

                    for (i in 0..featuresList.size) {
                        if (i < featuresList.size) {
                            point = LatLng(
                                featuresList[i].geometry.coordinates[1],
                                featuresList[i].geometry.coordinates[0]
                            )
                            markerList.add(
                                MarkerOptions().position(point)
                                    .title(featuresList[i].properties.title)
                                    .snippet(featuresList[i].properties.mag.toString())
                            )
                        }
                    }

                    mMap.setOnInfoWindowClickListener(this@MapsActivity)

                    clusterManager = ClusterManager(applicationContext, mMap)
                    setupClusterManager()
                    blurLayout.visibility = View.GONE
                } else {

                }

        }
        )
    }

    fun getEarthquakeDetail(id: String) {
        val call: Call<Features> = ApiClient.getClient.getEartquakeDetail(id)
        call.enqueue(object : Callback<Features> {
            override fun onFailure(call: Call<Features>?, t: Throwable?) {
                Log.d("MapsActivity", "onFail")
            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<Features>?, response: Response<Features>?) =
                if (response!!.isSuccessful) {
                    val view = layoutInflater.inflate(R.layout.bottom_sheet_dialog, null)
                    val dialog = BottomSheetDialog(this@MapsActivity)

                    view.mag_txt.text = response.body().properties.mag.toString()
                    view.place_txt.text = response.body().properties.place
                    view.depth_txt.text = response.body().geometry.coordinates[2].toString() + " km"
                    val datetime =
                        response.body().properties.products.origin[0].properties.eventtime

                    val firstSeparated: List<String> = datetime.split("T")
                    val secondSeparated: List<String> = firstSeparated[1].split(".")
                    val thirdSepareted: List<String> = secondSeparated[0].split(":")
                    view.date_txt.text =
                        thirdSepareted[0]+":"+thirdSepareted[1]

                    if (response.body().properties.tsunami == 1)
                        view.tsunami_txt.text = "Tsunami Warning!"
                    else view.tsunami_txt.text = "No Tsunami Alert"


                    dialog.setContentView(view)
                    dialog.show()
                } else {
                }


        })

    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.mMap = googleMap

//        if (ActivityCompat.checkSelfPermission(
//                this,
//                Manifest.permission.ACCESS_FINE_LOCATION
//            ) == PackageManager.PERMISSION_GRANTED
//        ) {
//
//            this.mMap.isMyLocationEnabled = true
//        } else {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                requestPermissions(
//                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
//                    MapsActivity.REQUEST_lOCATION
//                )
//            }
//        }
        try {

            val success = googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    this, R.raw.maps
                )
            )

            if (!success) {
                Log.e("MapsActivity", "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e("MapsActivity", "Can't find style. Error: ", e)
        }

    }

    override fun onBackPressed() {
        customDialog()
    }
    private fun customDialog() {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_design, null)
        val mBuilder = androidx.appcompat.app.AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        mDialogView.ok_btn.setOnClickListener {
           finishAffinity()
        }
        mDialogView.cancel_btn.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
//        if (requestCode == REQUEST_lOCATION) {
//            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                if (ActivityCompat.checkSelfPermission(
//                        this,
//                        Manifest.permission.ACCESS_FINE_LOCATION
//                    ) == PackageManager.PERMISSION_GRANTED
//                ) {
//                    mMap.isMyLocationEnabled = true
//                }
//            } else {
//                Toast.makeText(applicationContext, "Konum izni verilmedi.", Toast.LENGTH_SHORT)
//                    .show()
//            }
//        }
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    private fun setupClusterManager() {

        clusterManager.renderer = MarkerClusterRenderer(this, mMap, clusterManager)

        addClusters()

        mMap.setOnCameraIdleListener(clusterManager)
        mMap.setOnMarkerClickListener(clusterManager)
        clusterManager.cluster()
    }

    private fun addClusters() {
        for (marker in markerList) {
            val clusterItem = MyItem(marker.position, marker.title, marker.snippet)
            clusterManager.addItem(clusterItem)
        }
    }


    companion object {
        val REQUEST_lOCATION = 90
    }

    override fun onInfoWindowClick(p0: Marker?) {

        for (i in 0..featuresList.size) {
            if (i < featuresList.size) {
                if (p0?.position?.latitude == featuresList[i].geometry.coordinates[1]) {

                    getEarthquakeDetail(featuresList[i].id)


                }
            }
        }


    }

    override fun onClick(v: View?) {
        when (v?.id) {
            goBtn.id -> {
                val locationEt = findViewById(R.id.location_et) as EditText
                val location = locationEt.text.toString()
                if (location != null && location != "") {
                    var adressList: List<Address>? = null
                    val geocoder = Geocoder(this@MapsActivity)
                    try {
                        adressList = geocoder.getFromLocationName(location, 1)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                    val address = adressList!![0]
                    val latLng = LatLng(address.latitude, address.longitude)
                    mMap.addMarker(MarkerOptions().position(latLng).title("$location"))
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng))
                }
            }
            listBtn.id -> {
                // val intent = Intent(this, ListEarthquakeActivity::class.java)
                val intent = Intent(this, ListEarthquakeActivity::class.java)
              //  intent.putExtra("keyIdentifier", value)

                startActivity(intent)
            }
        }
    }

}
