package com.senler.ayca.earthquake

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class MyItem(val latLng: LatLng, val markerTitle: String = "", val snipped : String): ClusterItem{
    override fun getSnippet(): String {
        return snipped
    }

    override fun getTitle(): String {
        return markerTitle
    }

    override fun getPosition(): LatLng {
        return latLng
    }

}