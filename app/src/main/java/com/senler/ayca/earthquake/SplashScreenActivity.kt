package com.senler.ayca.earthquake

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.rbddevs.splashy.Splashy

class SplashScreenActivity : AppCompatActivity() {
    private val SPLASH_TIME_OUT: Long = 6000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen_activity)

        Splashy(this)
            .setLogo(R.drawable.earthquake)
            .setAnimation(Splashy.Animation.GLOW_LOGO)
            .setLogoWHinDp(100  ,100)
            .setBackgroundResource(R.color.colorPrimaryNight)
            .showTitle(false)
            .setTime(5000)
            .show()

        Splashy.onComplete(object : Splashy.OnComplete {
            override fun onComplete() {
                startActivity(Intent(this@SplashScreenActivity, MainActivity::class.java))

                finish()
            }

        })

    }
}