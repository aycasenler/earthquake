package com.senler.ayca.earthquake.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.senler.ayca.earthquake.R
import com.senler.ayca.earthquake.model.Features
import com.senler.ayca.earthquake.retrofit.ApiClient

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EarthquakeListAdapter(
    private var earthquakeList: List<Features>,
    private var context: Context
) :
    RecyclerView.Adapter<EarthquakeListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.list_earthquake_line_design, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return earthquakeList.size
    }

    override fun onBindViewHolder(holder: EarthquakeListAdapter.ViewHolder, position: Int) {

        val earthquakes = earthquakeList[position]
        holder.placeTxt.text = earthquakes.properties.place
        holder.magTxt.text = earthquakes.properties.mag.toString()

        getEarthquakeDetail(holder, earthquakes.id)


    }

    fun getEarthquakeDetail(holder: EarthquakeListAdapter.ViewHolder, id: String) {
        val call: Call<Features> = ApiClient.getClient.getEartquakeDetail(id)
        call.enqueue(object : Callback<Features> {
            override fun onFailure(call: Call<Features>?, t: Throwable?) {
                Log.d("EarthquakeListAdapter", "onFail")
            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<Features>?, response: Response<Features>?) =
                if (response!!.isSuccessful) {

                    val datetime =
                        response.body().properties.products.origin[0].properties.eventtime

                    val firstSeparated: List<String> = datetime.split("T")
                    val secondSeparated: List<String> = firstSeparated[1].split(".")
                    val thirdSepareted: List<String> = secondSeparated[0].split(":")
                    holder.dateTxt.text =
                        thirdSepareted[0]+":"+thirdSepareted[1]
                } else {
                }
        })

    }


    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var placeTxt: TextView = itemLayoutView.findViewById(R.id.place_txt)
        var magTxt: TextView = itemLayoutView.findViewById(R.id.mag_txt)
        var dateTxt: TextView = itemLayoutView.findViewById(R.id.date_txt)

    }


}