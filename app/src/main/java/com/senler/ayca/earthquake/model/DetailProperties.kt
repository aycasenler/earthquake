package com.senler.ayca.earthquake.model

import com.google.gson.annotations.SerializedName

data class DetailProperties(
    @SerializedName("eventtime")
    val eventtime : String
)