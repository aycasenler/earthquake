package com.senler.ayca.earthquake.model

import com.google.gson.annotations.SerializedName

data class Features(
    @SerializedName("properties")
    val properties : Properties,
    @SerializedName("geometry")
    val geometry : Geometry,
    @SerializedName("id")
    val id : String
)