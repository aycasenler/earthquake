package com.senler.ayca.earthquake.model

import com.google.gson.annotations.SerializedName

data class MainData(
    @SerializedName("features")
    val features : List<Features>
)