package com.senler.ayca.earthquake.model

import com.google.gson.annotations.SerializedName

data class Origin(
@SerializedName("properties")
val properties : DetailProperties
)