package com.senler.ayca.earthquake.model

import com.google.gson.annotations.SerializedName

data class Products(
    @SerializedName("origin")
    val origin : List<Origin>
)