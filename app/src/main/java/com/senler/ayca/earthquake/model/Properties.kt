package com.senler.ayca.earthquake.model

import com.google.gson.annotations.SerializedName

data class Properties(
    @SerializedName("mag")
    val mag : Double,
    @SerializedName("place")
    val place : String,
    @SerializedName("alert")
    val alert : String,
    @SerializedName("tsunami")
    val tsunami : Int,
    @SerializedName("type")
    val type : String,
    @SerializedName("title")
    val title : String,
    @SerializedName("products")
    val products : Products,
    @SerializedName("felt")
    val felt : Int
)