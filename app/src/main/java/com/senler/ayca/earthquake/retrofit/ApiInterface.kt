package com.senler.ayca.earthquake.retrofit


import com.senler.ayca.earthquake.model.Features
import com.senler.ayca.earthquake.model.MainData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    @GET("earthquakes/feed/v1.0/summary/all_day.geojson")
    fun getEarthquake(): Call<MainData>

    @GET("fdsnws/event/1/query?&format=geojson")
    fun getEartquakeDetail(@Query("eventid") id: String): Call<Features>

    @GET("fdsnws/event/1/query?format=geojson&orderby=time")
    fun getEarthquakeByMag(@Query("minmagnitude") minmagnitude: Double): Call<MainData>
}